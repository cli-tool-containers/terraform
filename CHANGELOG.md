# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!--
## [Unreleased]
-->

## [0.11.13] - 2019-04-08
### Added
- Dockerfile for terraform

### Changed
- README.md is updated

<!--
## [0.0.0] - 2017-06-20
### Added
### Changed
### Fixed
### Removed
-->

<!--
[Unreleased]: https://github.com/olivierlacan/keep-a-changelog/compare/v1.0.0...HEAD
-->
[0.11.13]: https://gitlab.com/cli-tool-containers/terraform/releases/tag/v0.11.13

