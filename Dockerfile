FROM golang:alpine as builder
ENV GOPATH /go
RUN apk add --update git bash openssh

ENV TERRAFORM_VERSION=0.11.13
ENV TF_DEV=true
ENV TF_RELEASE=1

WORKDIR $GOPATH/src/github.com/hashicorp/terraform
RUN git clone --branch v${TERRAFORM_VERSION} --depth 1 https://github.com/hashicorp/terraform.git ./ && \
    /bin/bash scripts/build.sh

FROM alpine:3.9
RUN apk --no-cache add --update ca-certificates
COPY --from=builder /go/bin /usr/bin/

ENTRYPOINT ["terraform"]
