# Terraform Container

This image is an optimized version of the version [offered by Hashicorp](https://hub.docker.com/r/hashicorp/terraform). It uses [multiple stages](https://docs.docker.com/develop/develop-images/multistage-build/) during the building of the Docker image. The size is decreased from ~130MB to ~99MB, which can be significant when using it as a cli-tool.

## **TL;DR**
Clone this repository and run:
```bash
docker build -t terraform . && docker run --rm terraform version
```

## How-To

A container running Terraform. Instead of installing and maintaining a CLI tool on you environment, you can use this container to execute your Terraform projects.

### Build

Build the docker container:

```bash
docker build -t terraform:0.11.13 .
```

### Run

Then use the container image like this:
```bash
docker run --rm terraform:0.11.13 <your-terraform-command>
```

For instance, see the `terraform version` result by running:
```bash
docker run --rm terraform:0.11.13 version
```

Additionally you can give the container access to the current directory:
```bash
docker run --rm -v $(pwd):/current-directory -w="/current-directory" terraform:0.11.13 <your-terraform-command>
```

### Tips&Tricks

You can add the following line to your `.bash_profile`:
```bash
alias terraform="docker run --rm -v $(pwd):/current-dir -w="/current-directory" terraform:0.11.13"
```

## Versioning

The [versioning scheme of Terraform](https://releases.hashicorp.com/terraform/) is used, however, only versions that were tested in images are published. For the versions available, see the [tags on this repository](https://gitlab.com/cli-tool-containers/terraform/tags) or the [CHANGELOG.md](CHANGELOG.md).

## Authors

* **Michiel Bakker** - *Owner/maintainer* - [GitLab profile](https://gitlab.com/mvbakker)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

## Acknowledgments

* The Hashicorp Terraform [repository](https://github.com/hashicorp/terraform)
* [This README template](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2) by PurpleBooth
